//
//  FlightStatusTableViewCell.m
//  FlightStatus
//
//  Created by David M Mast on 4/24/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "FlightStatusDetailViewCell.h"

@implementation FlightStatusDetailViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
