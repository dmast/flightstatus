// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Airport.m instead.

#import "_Airport.h"


const struct AirportAttributes AirportAttributes = {
	.citystate = @"citystate",
	.code = @"code",
	.country = @"country",
};








@implementation AirportID
@end

@implementation _Airport

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Airport" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Airport";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Airport" inManagedObjectContext:moc_];
}

- (AirportID*)objectID {
	return (AirportID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic citystate;






@dynamic code;






@dynamic country;











#if TARGET_OS_IPHONE

#endif

@end




