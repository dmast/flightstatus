// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Airport.h instead.

#import <CoreData/CoreData.h>



extern const struct AirportAttributes {
	__unsafe_unretained NSString *citystate;
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *country;
} AirportAttributes;
















@interface AirportID : NSManagedObjectID {}
@end

@interface _Airport : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (AirportID*)objectID;





@property (nonatomic, strong) NSString* citystate;



//- (BOOL)validateCitystate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* code;



//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* country;



//- (BOOL)validateCountry:(id*)value_ error:(NSError**)error_;






#if TARGET_OS_IPHONE

#endif

@end



@interface _Airport (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveCitystate;
- (void)setPrimitiveCitystate:(NSString*)value;




- (NSString*)primitiveCode;
- (void)setPrimitiveCode:(NSString*)value;




- (NSString*)primitiveCountry;
- (void)setPrimitiveCountry:(NSString*)value;




@end
