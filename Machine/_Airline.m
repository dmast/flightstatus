// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Airline.m instead.

#import "_Airline.h"


const struct AirlineAttributes AirlineAttributes = {
	.code = @"code",
	.name = @"name",
};








@implementation AirlineID
@end

@implementation _Airline

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Airline" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Airline";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Airline" inManagedObjectContext:moc_];
}

- (AirlineID*)objectID {
	return (AirlineID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic code;






@dynamic name;











#if TARGET_OS_IPHONE

#endif

@end




