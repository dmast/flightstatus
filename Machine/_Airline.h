// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Airline.h instead.

#import <CoreData/CoreData.h>



extern const struct AirlineAttributes {
	__unsafe_unretained NSString *code;
	__unsafe_unretained NSString *name;
} AirlineAttributes;














@interface AirlineID : NSManagedObjectID {}
@end

@interface _Airline : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (AirlineID*)objectID;





@property (nonatomic, strong) NSString* code;



//- (BOOL)validateCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;






#if TARGET_OS_IPHONE

#endif

@end



@interface _Airline (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveCode;
- (void)setPrimitiveCode:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




@end
