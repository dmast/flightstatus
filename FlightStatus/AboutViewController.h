//
//  AboutViewController.h
//  FlightStatusApp
//
//  Created by David Mast on 3/12/2014.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutViewController : UIViewController

@property (nonatomic, strong) IBOutlet UIWebView *webView;

@end
