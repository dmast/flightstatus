//
//  Utility.m
//  FlightStatus
//
//  Created by David M Mast on 5/21/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "Utility.h"

@implementation Utility

static NSString *departureText = @"i.e. ORD or Chicago";
static NSString *carrierText = @"i.e American Airlines";
static NSString *arrivalText = @"i.e. JFK or New York";
static BOOL bDeparture = NO;
static BOOL bArrival = NO;
static BOOL bCarrier = NO;

+ (NSString*)getDepartureText {
    return departureText;
}

+ (void)setDepartureText:(NSString*)newText {
    departureText = newText;
}

+ (NSString*)getCarrierText {
    return carrierText;
}

+ (void)setCarrierText:(NSString*)newText {
    carrierText = newText;
}

+ (NSString*)getArrivalText {
    return arrivalText;
}

+ (void)setArrivalText:(NSString*)newText {
    arrivalText = newText;
}


+ (BOOL)isCarrierSet {
    return bCarrier;
}

+ (void)setCarrier:(BOOL)value {
    bCarrier = value;
}

+ (BOOL)isArrivalSet {
    return bArrival;
}

+ (void)setArrival:(BOOL)value {
    bArrival = value;
}

+ (BOOL)isDepartureSet {
    return bDeparture;
}

+ (void)setDeparture:(BOOL)value {
    bDeparture = value;
}

+ (id)alloc {
    [NSException raise:@"Cannot be instantiated!" format:@"Static class 'ClassName' cannot be instantiated!"];
    return nil;
}

@end
