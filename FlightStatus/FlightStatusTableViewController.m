//
//  CustomTableViewController.m
//  CustomTable
//
//  Created by David Mast on 3/2/2014.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "FlightStatusTableViewController.h"
#import "FlightStatusTableCell.h"
#import "FlightStatusDetailViewController.h"
#import "FlightStatus.h"
#import "ObjectiveGumbo.h"
#import <CoreData/CoreData.h>
#import "Airport+Manage.h"
#import "Airline+Manage.h"
#import "FlightStatusAppDelegate.h"
#import "SearchViewController.h"
#import "Utility.h"
#import "MPAdView.h"
#import "MopubManager.h"



@interface FlightStatusTableViewController () <SSSearchBarDelegate, UITextFieldDelegate, UITextInputDelegate, UITableViewDataSource, UITableViewDelegate, SearchViewControllerDelegate, MPAdViewDelegate>
@property (strong, nonatomic) IBOutlet SSSearchBar *searchBar;
@property (nonatomic) NSArray *searchData;
@property (nonatomic) NSArray *data;
@property (nonatomic) NSString *queryType;
@property (nonatomic) BOOL isSearchForAirlines;
@property (nonatomic, retain) MPAdView *adView;

@end

@implementation FlightStatusTableViewController

{
    NSMutableArray *FlightStatusMutableArray;
    NSArray *searchResults;
    NSString *flightIDURL;
    NSArray *searchFlightStatusArray;
    NSMutableArray *pastFlights;
    NSMutableArray *pastFlightCodes;
    NSString *departureText;
    NSString *arrivalText;
    NSString *carrierText;
    BOOL updateArrivalText;
    BOOL updateDepartureText;
    BOOL updateCarrierText;
    NSMutableData *responseData;
    UIActivityIndicatorView *ui;
}
@synthesize FlightStatusArray;
@synthesize spinner;
@synthesize btnDeparture;
@synthesize btnArrival;
@synthesize btnCarrier;
@synthesize fetchedResultsController;
@synthesize managedObjectContext;
@synthesize departureTap;
@synthesize departureTransparentView;
@synthesize arrivalTap;
@synthesize arrivalTransparentView;
@synthesize isSearchForAirlines;
@synthesize carrierTransparentView;
@synthesize carrierTap;
@synthesize selectedDataFromSearch;
@synthesize lblCarrier;
@synthesize lblArrival;
@synthesize lblDeparture;


-(void)selectedFlightData:(SearchViewController*)controller didFinishSelectingData:(NSString*)data isSettingArrival:(BOOL)isArrival isSettingDeparture:(BOOL)isDeparture isSettingCarrier:(BOOL)isCarrier
{
    NSLog(@"Data Returned from Search= %@", data);
    if(isArrival)
    {
        arrivalText = data;
        updateArrivalText = YES;
        updateDepartureText = NO;
        updateCarrierText = NO;
        UILabel *dataLabel = [[UILabel alloc] initWithFrame:CGRectMake(112, 88, 188, 28)];
        dataLabel.text = data;
        dataLabel.backgroundColor = [UIColor greenColor];
        [arrivalTransparentView addSubview:dataLabel];
        [arrivalTransparentView setBackgroundColor:[UIColor greenColor]];
        
        
    }
    else if(isDeparture)
    {
        departureText = data;
        dispatch_async(dispatch_get_main_queue(),^ {
            UILabel *dataLabel = [[UILabel alloc] initWithFrame:CGRectMake(109, 45, 188, 28)];
            dataLabel.text = data;
            dataLabel.backgroundColor = [UIColor greenColor];
            [arrivalTransparentView addSubview:dataLabel];
            [arrivalTransparentView setBackgroundColor:[UIColor greenColor]];
            [self.view bringSubviewToFront:arrivalTransparentView];
        } );
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            UILabel *dataLabel = [[UILabel alloc] initWithFrame:CGRectMake(109, 45, 188, 28)];
            dataLabel.text = data;
            dataLabel.backgroundColor = [UIColor greenColor];
            [arrivalTransparentView addSubview:dataLabel];
            [arrivalTransparentView setBackgroundColor:[UIColor greenColor]];
            
            
            
        }];
    }
    else if(isCarrier)
    {
        carrierText = data;
        updateArrivalText = NO;
        updateDepartureText = NO;
        updateCarrierText = YES;
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            
            UILabel *dataLabel = [[UILabel alloc] initWithFrame:CGRectMake(109, 131, 188, 28)];
            dataLabel.text = data;
            dataLabel.backgroundColor = [UIColor greenColor];
            [arrivalTransparentView addSubview:dataLabel];
            [arrivalTransparentView setBackgroundColor:[UIColor greenColor]];
            
        }];
        
        
    }
    
}


- (void)saveFlightID:(NSString *)flightIDCode
{
    // Store the data
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lastFlightCode = [defaults objectForKey:@"lastFlightID"];
    [defaults setObject:lastFlightCode forKey:@"lastFlightID"];
    [defaults synchronize];
    
    NSLog(@"Data saved");
}
- (NSString*)loadFlightIDs
{
    // Store the data
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lasFilghtID = [defaults objectForKey:@"lastFlightID"];
    return lasFilghtID;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self performSegueWithIdentifier:@"showFlightStatusDetail" sender:self];
}

- (UIViewController *)viewControllerForPresentingModalView {
    return self;
}

- (void)viewWillLayoutSubviews {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [spinner setCenter:CGPointMake(screenWidth/2.0, screenHeight/2.0)]; // I do this because I'm in landscape mode
    [self.view addSubview:spinner];
    //self.navigationBar.barTintColor = [UIColor orangeColor];
    self.navigationController.navigationBar.barTintColor = [[UIColor alloc] initWithRed:0.08235294117647059 green:0.5803921568627451 blue:0.16862745098039217 alpha:.7];    //self.navigationController.navigationBar.barTintColor = [[UIColor alloc] initWith]
    //self.navigationController.navigationBar.tintColor = [UIColor blueColor];
    self.navigationController.navigationBar.alpha = 0.5f;
    self.navigationController.navigationBar.translucent = YES;
    //[self.btnDeparture setText:@"Hello World"];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    //self.navigationController.navigationBar.translucent = NO;
    
    
    lblCarrier.text = [Utility getCarrierText];
    lblArrival.text = [Utility getArrivalText];
    lblDeparture.text = [Utility getDepartureText];
    if([Utility isDepartureSet])
    {
        
        btnDeparture.hidden = YES;
        lblDeparture.hidden = NO;
        
    }
    if([Utility isArrivalSet])
    {
        
        btnArrival.hidden = YES;
        lblArrival.hidden = NO;
        
    }
    if([Utility isCarrierSet])
    {
        
        btnCarrier.hidden = YES;
        lblCarrier.hidden = NO;
        
    }
    
    //[self.adView loadAd];
    
    
}

-(void)resetSearchResourcesFrames
{
    
    //self.searchDisplayController.searchBar.frame = CGRectMake(0, 44, 352, 80);
    //self.searchController.searchResultsTableView.frame = CGRectMake(755, 100, 250, 400);
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"View Will appear");
    lblCarrier.text = [Utility getCarrierText];
    lblArrival.text = [Utility getArrivalText];
    lblDeparture.text = [Utility getDepartureText];
    if([Utility isDepartureSet])
    {
        
        [btnDeparture removeFromSuperview];
        lblDeparture.hidden = NO;
        
    }
    if([Utility isArrivalSet])
    {
        
        [btnArrival removeFromSuperview];
        lblArrival.hidden = NO;
        
    }
    
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    
}

- (void)viewDidLoad
{
    
    self.adView = MopubManager.SingletonMopubPhoneBannerAdview;
    self.adView.delegate = self;
    self.adView.frame = CGRectMake(0, (self.view.frame.size.height - MOPUB_BANNER_SIZE.height)-65,MOPUB_BANNER_SIZE.width, MOPUB_BANNER_SIZE.height);
    [self.view addSubview:self.adView];
    [self.adView loadAd];
    [super viewDidLoad];
    self.searchBar.cancelButtonHidden = NO;
    self.searchBar.placeholder = NSLocalizedString(@"Enter Flight Code i.e. \"ua401\"", nil);
    self.searchBar.delegate = self;
    //[self.searchBar becomeFirstResponder];
    
    self.data = @[ @"Hey there!", @"This is a custom UISearchBar.", @"And it's really easy to use...", @"Sweet!" ];
    self.searchData = self.data;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.isSearchForAirlines = YES;
    departureText = @"i.e. ORD or Chicago";
    
    arrivalText = @"i.e. JFK or New York";
    carrierText = @"i.e. American Airlines";
    lblCarrier.hidden = true;
    
    /**
     [profileAreaView addGestureRecognizer:self.tapProfileGesture];
     [homeAreaView addGestureRecognizer:self.tapHomeGesture];
     **/
    [departureTransparentView addGestureRecognizer:self.departureTap];
    [arrivalTransparentView addGestureRecognizer:self.arrivalTap];
    [carrierTransparentView addGestureRecognizer:self.carrierTap];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(modelObjectUpdatedString:) name:@"DoUpdateCarrier" object:nil];
    

    
}

- (void)modelObjectUpdatedString:(NSNotification *)notification
{
    //NSObject *postingObject = [notification object];
    dispatch_async(dispatch_get_main_queue(), ^{
        NSString *string = [[notification userInfo]
                            objectForKey:@"String"];
        NSLog(@"%@", string);
        lblCarrier.text = string;
    });
    
    
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([searchResults count] > 0) {
        return [searchResults count];
        
    }
    else if([FlightStatusArray count] >0)
    {
        if([FlightStatusArray count] >2)
        {
            
            return 2;
        }
        else
        {
            
            return [FlightStatusArray count];
        }
    }
    else {
        
        //return [FlightStatusArray count];
        //return 4;
        /**
         NSOperationQueue* operationQueue = [[NSOperationQueue alloc] init];
         [operationQueue addOperationWithBlock:^{
         FlightStatusArray = [self parseFlightStatus:@"ua401"];
         //return [FlightStatusArray count];
         //return [FlightStatusArray count];
         }];
         **/
        
        
        //NSArray *recentSearches = [self loadFlightIDs];
        return 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomTableCell";
    FlightStatusTableCell *cell = (FlightStatusTableCell *)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[FlightStatusTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Display FlightStatus in the table cell
    FlightStatus *flightStatus = nil;
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        flightStatus = [searchResults objectAtIndex:indexPath.row];
    } else {
        flightStatus = [FlightStatusArray objectAtIndex:indexPath.row];
    }
    if(flightStatus)
    {
        cell.flightID.text = [NSString stringWithFormat:@"%@ %@ to %@",flightStatus.flightID,flightStatus.departureAirportCode, flightStatus.arrivalAirportCode  ];
        cell.flightStatus.text = flightStatus.status;
        cell.flightStatusDate.text = flightStatus.departureDate;
        
        if (!([flightStatus.status rangeOfString:@"On-time"].location == NSNotFound)
            || !([flightStatus.status rangeOfString:@"Departed"].location == NSNotFound)
            || !([flightStatus.status rangeOfString:@"Landed"].location == NSNotFound)
            ) {
            //[cell.flightImage setImage:[UIImage imageNamed: @"onTimeFlightImage.png"]];
            cell.flightStatus.textColor = [UIColor greenColor];
        }
        else
        {
            //[cell.flightImage setImage:[UIImage imageNamed: @"delayedFlightImage.png"
            cell.flightStatus.textColor = [UIColor redColor];
        }
    }
    else
    {
        UIActivityIndicatorView *spinner3 = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        //spinner3.color = [UIColor blackColor];
        cell.accessoryView = spinner3;
        [spinner3 startAnimating];
        
        //self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:spinner3];
        dispatch_queue_t downloadRecentFlights = dispatch_queue_create("flight downloader recent", NULL);
        
        dispatch_async(downloadRecentFlights, ^{
            NSString *lastFlightID = [self loadFlightIDs];
            NSArray *newSearchFlightStatusArray = [[NSArray alloc] init];
            if(lastFlightID)
            {
                 newSearchFlightStatusArray = [self parseFlightStatusByID:lastFlightID];
            }
            else
            {
                newSearchFlightStatusArray = [self parseFlightStatusByID:@"ua401"];
            }
            
            //[ui stopAnimating];
            dispatch_async(dispatch_get_main_queue(), ^{
                // put rightbar button back
                //self.navigationItem.rightBarButtonItem = sender; // sender because that's the element that called us by clicking refresh
                [spinner3 stopAnimating];
                if(newSearchFlightStatusArray.count > 0)
                {
                    searchFlightStatusArray = newSearchFlightStatusArray;
                    FlightStatusArray = newSearchFlightStatusArray;
                    for(FlightStatus *f in searchFlightStatusArray)
                    {
                        cell.flightID.text = [NSString stringWithFormat:@"%@ %@ to %@",f.flightID,f.departureAirportCode, f.arrivalAirportCode  ];
                        cell.flightStatus.text = f.status;
                        cell.flightStatusDate.text = f.departureDate;
                        if (!([f.status rangeOfString:@"On-time"].location == NSNotFound)
                            || !([f.status rangeOfString:@"Departed"].location == NSNotFound)
                            || !([f.status rangeOfString:@"Landed"].location == NSNotFound)
                            ) {
                            //[cell.flightImage setImage:[UIImage imageNamed: @"onTimeFlightImage.png"]];
                            cell.flightStatus.textColor = [UIColor greenColor];
                        }
                        else
                        {
                            //[cell.flightImage setImage:[UIImage imageNamed: @"delayedFlightImage.png"
                            cell.flightStatus.textColor = [UIColor redColor];
                        }
                        
                    }
                    //[tableView reloadData];
                }
                else
                {
                    cell.textLabel.text = @"No recent flight data found";
                }
                
            });
        });

    }
    
    return cell;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) {
        NSLog(@"Return or Enter button pressed");
        /**
         if(textField.tag == 1)
         {
         [textField resignFirstResponder];
         [btnArrival becomeFirstResponder];
         }
         if(textField.tag == 2)
         {
         [textField resignFirstResponder];
         [btnCarrier becomeFirstResponder];
         }
         **/
        
        
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.placeholder = textField.text;
    //textField.text = @"";
    if(textField.tag == 1)
    {
    }
    if(textField.tag == 2)
    {
        
    }
    if(textField.tag == 3)
    {
        
    }
    
}





- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [super prepareForSegue:segue sender:sender];
    if ([[segue identifier] isEqualToString:@"segueToSearch"]) {
        UINavigationController *navigationController = (UINavigationController *)segue.destinationViewController;
        SearchViewController *vc = [[navigationController viewControllers] lastObject];
        
        vc.isSearchForAirlines = self.isSearchForAirlines;
        vc.isSettingArrival = self.isSettingArrival;
        vc.isSettingDeparture = self.isSettingDeparture;
        vc.isSettingCarrier = self.isSettingCarrier;
        vc.delegate = self;
        //vc.queryType = _queryType;
        
        
    }
    
    if ([segue.identifier isEqualToString:@"showFlightStatusDetail"]) {
        NSIndexPath *indexPath = nil;
        FlightStatus *flightStatus = nil;
        
        if (self.searchDisplayController.active) {
            indexPath = [self.searchDisplayController.searchResultsTableView indexPathForSelectedRow];
            //flightStatus = [searchResults objectAtIndex:indexPath.row];
            flightStatus = [FlightStatusArray objectAtIndex:indexPath.row];
        } else {
            indexPath = [self.tableView indexPathForSelectedRow];
            flightStatus = [FlightStatusArray objectAtIndex:indexPath.row];
        }
        UINavigationController *navigationController = (UINavigationController *)segue.destinationViewController;
        FlightStatusDetailViewController *vc = [[navigationController viewControllers] lastObject];
        // destViewController.FlightStatus = flightStatus;
        if(searchFlightStatusArray == nil)
        {
            NSArray *arr = [[NSArray alloc] initWithObjects:flightStatus, nil];
            vc.searchFlightStatusArray = arr;
        }
        else
        {
            vc.searchFlightStatusArray = searchFlightStatusArray;
        }
        //segueToAirportSelection
    }
}

- (void)filterContentForSearchText:(NSString*)searchText
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"flightID contains[c] %@", searchText];
    searchResults = [FlightStatusArray filteredArrayUsingPredicate:resultPredicate];
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBarInstance
{
    NSLog(@"User searched for %@", searchBarInstance.text);
    searchBarInstance.text = @"";
    
}
-(void)searchBar:(UISearchBar*)searchBarInstance textDidChange:(NSString*)text
{
    NSLog(@"performing fetch on search");
    //[self performFetch];
    NSLog(@"Text Changed %@", searchBarInstance.text);
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBarInstance {
    [self handleSearch:searchBarInstance];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBarInstance {
    //[self handleSearch:searchBarInstance];
}

- (void)handleSearch:(UISearchBar *)searchBarInstance {
    NSLog(@"User searched for %@", searchBarInstance.text);
    
    NSString *flightCodeText = [searchBarInstance.text stringByReplacingOccurrencesOfString: @" " withString:@""];
    //NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"flightID contains[c] %@", searchBarInstance.text];
    //FlightStatusArray = [FlightStatusArray filteredArrayUsingPredicate:resultPredicate];
    
    
    UIActivityIndicatorView *spinner1 = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner1.color = [UIColor blackColor];
    [spinner1 startAnimating];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:spinner1];
    dispatch_queue_t downloadQueue = dispatch_queue_create("flight downloader", NULL);
    dispatch_async(downloadQueue, ^{
        NSArray *newSearchFlightStatusArray =  newSearchFlightStatusArray = [self parseFlightStatusByID:flightCodeText];
        //[ui stopAnimating];
        dispatch_async(dispatch_get_main_queue(), ^{
            // put rightbar button back
            //self.navigationItem.rightBarButtonItem = sender; // sender because that's the element that called us by clicking refresh
            [spinner1 stopAnimating];
            if(newSearchFlightStatusArray.count > 0)
            {
                searchFlightStatusArray = newSearchFlightStatusArray;
                [self saveFlightID:flightCodeText];
                //[self.navigationController pushViewController:flightStatusViewController animated:YES];
                [self performSegueWithIdentifier:@"showFlightStatusDetail" sender:self];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Flight Status"
                                                                message:[NSString stringWithFormat:@"No recent flight data found for that flight"]
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
        });
    });
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBarInstance {
    NSLog(@"User canceled search");
    [searchBarInstance resignFirstResponder]; // if you want the keyboard to go away
}

-(NSArray *)parseFlightStatusByID:(NSString *)flightID
{
    //NSString *fullURL = @"https://www.google.com/search?q=ua302&ie=utf-8&oe=utf-8&aq=t&rls=org.mozilla:en-US:official&client=firefox-a";
    flightIDURL = [NSString stringWithFormat:@"https://www.google.com/search?site=&source=hp&ei=y79NU7-pOsqusATS8YGQBw&q=%@&oq=%@&gs_l=mobile-gws-hp.3..0l2j0i30l2j0i5i30.8707.11504.0.12633.6.6.0.2.2.0.315.1095.1j2j2j1.6.0....0...1c.1.41.mobile-gws-hp..1.5.654.0.akbJl-seCfs",flightID, flightID];
    NSURL *url = [NSURL URLWithString:flightIDURL];
    NSString *webData= [NSString stringWithContentsOfURL:url];
    //NSLog(@"%@",webData);
    
    OGNode *data = [ObjectiveGumbo parseDocumentWithString:webData];
    
    NSArray *bodyRows = [data elementsWithTag:GUMBO_TAG_BODY];
    NSMutableArray *flightStatusARR = [[NSMutableArray alloc] init];
    for (OGElement * bodyRow in bodyRows)
    {
        
        NSArray *olRows = [bodyRow elementsWithTag:GUMBO_TAG_OL];
        for (OGElement * olRow in olRows)
        {
            //Get table 1 and table 2
            //NSLog(@"Ordered list HTML = %@", olRow.html);
            NSArray *ilRows = [olRow elementsWithTag:GUMBO_TAG_LI];
            int counter = 0;
            for (OGElement * flightStatusElement in ilRows)
            {
                if(counter == 0)
                {
                    // NSLog(@"iStatusElement HTML = %@", flightStatusElement.html);
                    NSArray *obTable = [flightStatusElement elementsWithClass:@"obcontainer"];
                    for (OGElement * tableInOrderList in obTable)
                    {
                        //NSLog(@"%@", tableInOrderList.html);
                        //NSLog(@"tableInOrderList HTML = %@", tableInOrderList.html);
                        NSArray *dataTables = [tableInOrderList elementsWithTag:GUMBO_TAG_TABLE];
                        int tableCounter =0;
                        NSString *flightID;
                        for (OGElement * flightData in dataTables)
                        {
                            
                            FlightStatus *flightStatusObj;
                            
                            if(tableCounter ==0)
                            {
                                
                                NSArray *flisghtStatusRow = [flightData elementsWithTag:GUMBO_TAG_TR];
                                for (OGElement * flightStatusCols in flisghtStatusRow)
                                {
                                    //NSLog(@"%@", flightStatus.html);
                                    //NSLog(@"%@", flightStatus.text);
                                    NSArray *flightStatus = [flightStatusCols elementsWithTag:GUMBO_TAG_TD];
                                    
                                    for (OGElement * fs in flightStatus)
                                    {
                                        NSArray *flightTitle = [fs elementsWithTag:GUMBO_TAG_B];
                                        
                                        for (OGElement * fsTitle in flightTitle)
                                        {
                                            //NSLog(@"%@", fsTitle.html);
                                            //NSLog(@"%@", fsTitle.text);
                                            flightStatusObj.flightID = fsTitle.text;
                                            flightID = fsTitle.text;
                                        }
                                        
                                    }
                                }
                                
                            }
                            if(tableCounter ==1)
                            {
                                //NSLog(@"FLIGHT DETAILS HTML = %@", flightData.html);
                                //NSLog(@"FLIGHT DETAILS TEXT = %@", flightData.text);
                                
                                NSArray *flightStatusRow = [flightData elementsWithTag:GUMBO_TAG_TD];
                                
                                int dataCounter =0;
                                
                                for (OGElement * flightStatusRowData in flightStatusRow)
                                {
                                    
                                    //NSLog(@"%D : %@",  dataCounter,flightStatusRowData.text);
                                    //Landed
                                    if (!([flightStatusRowData.text rangeOfString:@"On-time"].location == NSNotFound)
                                        || !([flightStatusRowData.text rangeOfString:@"Delayed"].location == NSNotFound)
                                        || !([flightStatusRowData.text rangeOfString:@"Departed"].location == NSNotFound)
                                        || !([flightStatusRowData.text rangeOfString:@"Canceled"].location == NSNotFound)
                                        || !([flightStatusRowData.text rangeOfString:@"Landed"].location == NSNotFound)
                                        ) {
                                        flightStatusObj = [[FlightStatus alloc] init];
                                        
                                        flightStatusObj.flightIDCode = flightID;
                                        
                                        //NSLog(@"On-time");
                                        OGElement *fsStatus = (OGElement*) flightStatusRow[dataCounter];
                                        flightStatusObj.status = fsStatus.text;
                                        
                                        OGElement *fsStatusMsg = (OGElement*) flightStatusRow[dataCounter + 1];
                                        flightStatusObj.statusMessage = fsStatusMsg.text;
                                        
                                        //OGElement *fsDepartureOrArrival = (OGElement*) flightStatusRow[dataCounter + 2];
                                        
                                    }
                                    
                                    
                                    
                                    OGElement *fsDepartureOrArrival = (OGElement*) flightStatusRow[dataCounter];
                                    if (!([fsDepartureOrArrival.text rangeOfString:@"Departure"].location == NSNotFound)) {
                                        if(dataCounter <= flightStatusRow.count)
                                        {
                                            flightStatusObj.departureAirportCodeText = fsDepartureOrArrival.text;
                                            OGElement *fsDepartureOrArrivalCode = (OGElement*) flightStatusRow[dataCounter + 1];
                                            flightStatusObj.departureAirportCode = fsDepartureOrArrivalCode.text;
                                            OGElement *fsDepartureDateTime = (OGElement*) flightStatusRow[dataCounter + 2];
                                            flightStatusObj.departureDateTime = fsDepartureDateTime.text;
                                            OGElement *fsDepartureScheduledDateTime = (OGElement*) flightStatusRow[dataCounter + 3];
                                            if (!([fsDepartureScheduledDateTime.text rangeOfString:@"(scheduled"].location == NSNotFound)) {
                                                flightStatusObj.departureScheduledTimeText = fsDepartureScheduledDateTime.text;
                                            }
                                            
                                            OGElement *fsTerminal = (OGElement*) flightStatusRow[dataCounter +4];
                                            if (!([fsTerminal.text rangeOfString:@"Terminal"].location == NSNotFound)) {
                                                flightStatusObj.departureTerminal = fsTerminal.text;
                                            }
                                            
                                            
                                            OGElement *fsCity = (OGElement*) flightStatusRow[dataCounter + 6];
                                            flightStatusObj.departureCity = fsCity.text;
                                            
                                            OGElement *fsDateText = (OGElement*) flightStatusRow[dataCounter + 7];
                                            flightStatusObj.departureDate = fsDateText.text;
                                            if(dataCounter + 9 < flightStatusRow.count)
                                            {
                                                OGElement *fsGate = (OGElement*) flightStatusRow[dataCounter + 9];
                                                if (!([fsGate.text rangeOfString:@"Gate"].location == NSNotFound)) {
                                                    flightStatusObj.departureGate = fsGate.text;
                                                }
                                            }
                                            
                                        }
                                        
                                        
                                    }
                                    
                                    if (!([flightStatusRowData.text rangeOfString:@"Arrival"].location == NSNotFound)) {
                                        if(dataCounter <= flightStatusRow.count)
                                        {
                                            flightStatusObj.arrivalAirportText = fsDepartureOrArrival.text;
                                            OGElement *fsDepartureOrArrivalCode = (OGElement*) flightStatusRow[dataCounter + 1];
                                            flightStatusObj.arrivalAirportCode = fsDepartureOrArrivalCode.text;
                                            OGElement *fsArrivalDateTime = (OGElement*) flightStatusRow[dataCounter + 2];
                                            flightStatusObj.arrivalDateTime = fsArrivalDateTime.text;
                                            OGElement *fsArrivalScheduledDateTime = (OGElement*) flightStatusRow[dataCounter + 3];
                                            
                                            if (!([fsArrivalScheduledDateTime.text rangeOfString:@"(scheduled"].location == NSNotFound)) {
                                                flightStatusObj.arrivalScheduledTimeText = fsArrivalScheduledDateTime.text;
                                            }
                                            
                                            OGElement *fsTerminal = (OGElement*) flightStatusRow[dataCounter + 4];
                                            if (!([fsTerminal.text rangeOfString:@"Terminal"].location == NSNotFound)) {
                                                flightStatusObj.departureTerminal = fsTerminal.text;
                                            }
                                            
                                            OGElement *fsCity = (OGElement*) flightStatusRow[dataCounter + 6];
                                            flightStatusObj.arrivalCity = fsCity.text;
                                            
                                            OGElement *fsDateText = (OGElement*) flightStatusRow[dataCounter + 7];
                                            flightStatusObj.arrivalDate = fsDateText.text;
                                            if(dataCounter + 9 < flightStatusRow.count)
                                            {
                                                OGElement *fsGate = (OGElement*) flightStatusRow[dataCounter + 9];
                                                if (!([fsGate.text rangeOfString:@"Gate"].location == NSNotFound)) {
                                                    flightStatusObj.arrivalGate = fsGate.text;
                                                }
                                            }
                                            
                                            
                                        }
                                    }
                                    flightStatusObj.flightID = flightID;
                                    if(flightStatusObj.arrivalAirportCode != nil)
                                    {
                                        [flightStatusARR addObject:flightStatusObj];
                                        flightStatusObj = nil;
                                    }
                                    
                                    dataCounter++;
                                    
                                }
                                
                            }
                            
                            tableCounter++;
                        }
                        
                        
                    }
                }
                counter++;
            }
            
        }
        
        NSLog(@"Count of Flight status %lu", (unsigned long)flightStatusARR.count);
    }
    return flightStatusARR;
    //FlightStatusTableViewController.managedObjectContext
}

- (NSArray *)parseFlightStatus:(NSString *)departureCode arrivalCode:(NSString *)arrivalCode dateForFlight:(NSString *)dateForFlight airline:(NSString *)airline departureCity:(NSString*)departureCity arrivalCity:(NSString*)arrivalCity
{
    //2014-05-22
    
    flightIDURL = [NSString stringWithFormat:@"http://www.flightstats.com/go/FlightStatus/flightStatusByRoute.do;?departure=%@&arrival=%@&departureDate=%@&airlineToFilter=%@",departureCode,arrivalCode, dateForFlight,airline];
    NSString* userAgent = @"Mozilla/5.001 (windows; U; NT4.0; en-US; rv:1.0) Gecko/25250101";
    NSURL* url = [NSURL URLWithString:flightIDURL];
    NSMutableURLRequest* request = [[NSMutableURLRequest alloc] initWithURL:url];
    [request addValue:userAgent forHTTPHeaderField:@"User-Agent"];
    
    NSURLResponse* response = nil;
    NSError* error = nil;
    NSData* dataFromURL = [NSURLConnection sendSynchronousRequest:request
                                                returningResponse:&response
                                                            error:&error];
    
    NSString* newStr = [[NSString alloc] initWithData:dataFromURL encoding:NSUTF8StringEncoding];
    OGNode *data = [ObjectiveGumbo parseDocumentWithString:newStr];
    
    
    //NSArray *bodyRows = [data elementsWithTag:GUMBO_TAG_BODY];
    
    NSArray *flightStatusTable = [data elementsWithClass:@"tableListingTable"];
    NSMutableArray *flightStatusARR = [[NSMutableArray alloc] init];
    for(OGElement *tableData in flightStatusTable)
    {
        //NSLog(@"%@",tableData.html);
        NSArray *flisghtStatusRow = [tableData elementsWithTag:GUMBO_TAG_TR];
        int counter = 0;
        for(OGElement *flightStatusRowData in flisghtStatusRow)
        {
            //NSLog(@"Row:%d %@",counter,flightStatusRowData.text);
            counter++;
            if(counter >1)
            {
                int tdCounter = 0;
                NSArray *flightStatusRowDataContents = [flightStatusRowData elementsWithTag:GUMBO_TAG_TD];
                if([flightStatusRowDataContents count] > 0)
                {
                    FlightStatus *fs = [[FlightStatus alloc] init];
                    fs.departureCity = departureCity;
                    fs.departureAirportCode = departureCode;
                    fs.departureAirportCodeText = departureCode;
                    fs.arrivalCity = arrivalCity;
                    fs.arrivalAirportCode = arrivalCode;
                    fs.arrivalAirportText = arrivalCode;
                    //OGElement *fsGate = (OGElement*) flightStatusRow[dataCounter + 9];
                    OGElement *fsFlightID = (OGElement*) flightStatusRowDataContents[0];
                    fs.flightID = [fsFlightID.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    OGElement *fsFlightCarrier = (OGElement*) flightStatusRowDataContents[2];
                    fs.carrier = [fsFlightCarrier.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    OGElement *fsDS = (OGElement*) flightStatusRowDataContents[3];
                    fs.departureScheduledTimeText = [fsDS.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    OGElement *fsDSACT = (OGElement*) flightStatusRowDataContents[4];
                    fs.departureDateTime = [fsDSACT.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    fs.departureDate = [fsDSACT.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    OGElement *fsDTERM = (OGElement*) flightStatusRowDataContents[5];
                    fs.departureGate = [fsDTERM.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    fs.departureTerminal = [fsDTERM.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    OGElement *fsAS = (OGElement*) flightStatusRowDataContents[6];
                    fs.arrivalScheduledTimeText = [fsAS.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    OGElement *fsACTAS = (OGElement*) flightStatusRowDataContents[7];
                    fs.arrivalDate = [fsACTAS.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    fs.arrivalDateTime = [fsACTAS.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    OGElement *fsATERM = (OGElement*) flightStatusRowDataContents[8];
                    fs.arrivalTerminal = [fsATERM.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    fs.arrivalGate = [fsATERM.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    OGElement *fsSTATUS = (OGElement*) flightStatusRowDataContents[9];
                    fs.status = [fsSTATUS.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    fs.statusMessage = [fsSTATUS.text stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                    
                    [flightStatusARR addObject:fs];
                    /**
                     for(OGElement *flightStatusRowDataElement in flisghtStatusRowData)
                     {
                     
                     NSLog(@"TD_COUNTER:%d %@",tdCounter,flightStatusRowDataElement.text);
                     tdCounter++;
                     }
                     **/
                }
            }
            
        }
        
    }
    
    
    
    return flightStatusARR;
    //FlightStatusTableViewController.managedObjectContext
}

- (IBAction)destinationTapped:(id)sender {
    NSLog(@"Departure has been touched");
    self.isSearchForAirlines = NO;
    self.isSettingDeparture= YES;
    self.isSettingArrival = NO;
    self.isSettingCarrier = NO;
    [self performSegueWithIdentifier:@"segueToSearch" sender:self];
    
    
    
    
}

- (IBAction)arrivalTapped:(id)sender {
    self.isSearchForAirlines = NO;
    self.isSettingArrival = YES;
    self.isSettingCarrier = NO;
    self.isSettingDeparture = NO;
    NSLog(@"Arrival has been touched");
    [self performSegueWithIdentifier:@"segueToSearch" sender:self];
    
    
}


- (IBAction)carrierDidTap:(id)sender {
    self.isSearchForAirlines = YES;
    self.isSettingCarrier = YES;
    self.isSettingDeparture = NO;
    self.isSettingArrival = NO;
    NSLog(@"Carrier has been touched");
    [self performSegueWithIdentifier:@"segueToSearch" sender:self];
}
- (IBAction)searchClicked:(id)sender {
    /**
     q=American+Airlines+ORD+JFK&oq=American+Airlines+ORD+JFK&
     **/
    
    NSString *departureCode =[lblDeparture.text substringFromIndex: [lblDeparture.text length] - 4];
    NSString *updatedDepartureCode = [departureCode stringByReplacingOccurrencesOfString: @")" withString:@""];
    NSString *arrivalCode =[lblArrival.text substringFromIndex: [lblArrival.text length] - 4];
    NSString *updatedArrivalCode = [arrivalCode stringByReplacingOccurrencesOfString: @")" withString:@""];
    
    NSString *carrierCode = [lblCarrier.text substringFromIndex: [lblCarrier.text length] - 3];
    NSString *updatedCarrierCode = [carrierCode stringByReplacingOccurrencesOfString: @")" withString:@""];
    
    //NSString *searchquery = [NSString stringWithFormat:@"%@+%@+%@", carrierCode, updatedDepartureCode, updatedArrivalCode];
    
    NSDate *date = [[NSDate alloc]init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSLog(@"%@", [formatter stringFromDate:date]);
    
    UIActivityIndicatorView *spinner2 = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner2.color = [UIColor blackColor];
    [spinner2 startAnimating];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:spinner2];
    dispatch_queue_t downloadQueue = dispatch_queue_create("flight downloader", NULL);
    
    dispatch_async(downloadQueue, ^{
        NSArray *newSearchFlightStatusArray =  newSearchFlightStatusArray = [self parseFlightStatus:updatedDepartureCode arrivalCode:updatedArrivalCode dateForFlight:[formatter stringFromDate:date] airline:updatedCarrierCode departureCity:lblDeparture.text arrivalCity:lblArrival.text];
        //[ui stopAnimating];
        dispatch_async(dispatch_get_main_queue(), ^{
            // put rightbar button back
            //self.navigationItem.rightBarButtonItem = sender; // sender because that's the element that called us by clicking refresh
            [spinner2 stopAnimating];
            if(newSearchFlightStatusArray.count > 0)
            {
                //NSSortDescriptor* sortOrder = [NSSortDescriptor sortDescriptorWithKey: @"self" ascending: NO];
                //searchFlightStatusArray = [newSearchFlightStatusArray sortedArrayUsingDescriptors: [NSArray arrayWithObject: sortOrder]];
                //searchFlightStatusArray =newSearchFlightStatusArray
                //NSArray *sortedPersonArray = [coreDataPersonArray sortedArrayUsingSelector:@selector(compare:)];
               
                searchFlightStatusArray =newSearchFlightStatusArray;
                //[self saveFlightID:flightCodeText];
                //[self.navigationController pushViewController:flightStatusViewController animated:YES];
                [self performSegueWithIdentifier:@"showFlightStatusDetail" sender:self];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Flight Status"
                                                                message:[NSString stringWithFormat:@"No recent flight data found for that flight"]
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
        });
    });
    
    
    // do any UI stuff on the main UI thread
    
    
    
}
/**
 - (void)load {
 NSURL *myURL = [NSURL URLWithString:@""];
 NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:myURL
 cachePolicy:NSURLRequestReloadIgnoringLocalCacheData
 timeoutInterval:60];
 [request ]
 
 [[NSURLConnection alloc] initWithRequest:request delegate:self];
 }
 
 - (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
 responseData = [[NSMutableData alloc] init];
 }
 
 - (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
 [responseData appendData:data];
 }
 
 - (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
 
 }
 
 - (void)connectionDidFinishLoading:(NSURLConnection *)connection
 {
 NSLog(@"Succeeded! Received %d bytes of data",[responseData length]);
 NSString *txt = [[NSString alloc] initWithData:responseData encoding:NSASCIIStringEncoding];
 
 }
 **/
@end
