//
//  MopubManager.h
//  FlightStatus
//
//  Created by David M Mast on 5/23/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MPAdView.h"


@interface MopubManager : NSObject {

    
}

+ (id)sharedMopubManager;
+ (id)SingletonMopubPhoneBannerAdview;
+ (id)SingletonMopubTabletBannerAdview;


@end
