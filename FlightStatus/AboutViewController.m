//
//  AboutViewController.m
//  FlightStatusApp
//
//  Created by David Mast on 3/12/2014.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "AboutViewController.h"
#import "ObjectiveGumbo.h"
#import "FlightStatus.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (NSArray *)parseFlightStatus:(NSString *)fullURL
{
    //NSString *fullURL = @"https://www.google.com/search?q=ua302&ie=utf-8&oe=utf-8&aq=t&rls=org.mozilla:en-US:official&client=firefox-a";
    NSURL *url = [NSURL URLWithString:fullURL];
    NSString *webData= [NSString stringWithContentsOfURL:url];
    //NSLog(@"%@",webData);
    
    OGNode *data = [ObjectiveGumbo parseDocumentWithString:webData];
    
    NSArray *bodyRows = [data elementsWithTag:GUMBO_TAG_BODY];
    NSMutableArray *flightStatusARR = [[NSMutableArray alloc] init];
    for (OGElement * bodyRow in bodyRows)
    {
        
        //NSLog(@"Row Number =  %d", counter);
        //NSLog(@"Body Text = %@", bodyRow.html);
        //OGNode *flightStatusNode = [ObjectiveGumbo parseNodeWithString:bodyRow.html];
        NSArray *olRows = [bodyRow elementsWithTag:GUMBO_TAG_OL];
        for (OGElement * olRow in olRows)
        {
            //Get table 1 and table 2
            //NSLog(@"Ordered list HTML = %@", olRow.html);
            NSArray *ilRows = [olRow elementsWithTag:GUMBO_TAG_LI];
            int counter = 0;
            for (OGElement * flightStatusElement in ilRows)
            {
                if(counter == 0)
                {
                    NSLog(@"iStatusElement HTML = %@", flightStatusElement.html);
                    NSArray *obTable = [flightStatusElement elementsWithClass:@"obcontainer"];
                    for (OGElement * tableInOrderList in obTable)
                    {
                        //NSLog(@"%@", tableInOrderList.html);
                        //NSLog(@"tableInOrderList HTML = %@", tableInOrderList.html);
                        NSArray *dataTables = [tableInOrderList elementsWithTag:GUMBO_TAG_TABLE];
                        int tableCounter =0;
                        NSString *flightID;
                        for (OGElement * flightData in dataTables)
                        {
                            
                            FlightStatus *flightStatusObj;
                            if(tableCounter ==0)
                            {
                                
                                NSArray *flisghtStatusRow = [flightData elementsWithTag:GUMBO_TAG_TR];
                                for (OGElement * flightStatusCols in flisghtStatusRow)
                                {
                                    //NSLog(@"%@", flightStatus.html);
                                    //NSLog(@"%@", flightStatus.text);
                                    NSArray *flightStatus = [flightStatusCols elementsWithTag:GUMBO_TAG_TD];
                                    
                                    for (OGElement * fs in flightStatus)
                                    {
                                        NSArray *flightTitle = [fs elementsWithTag:GUMBO_TAG_B];
                                        
                                        for (OGElement * fsTitle in flightTitle)
                                        {
                                            //NSLog(@"%@", fsTitle.html);
                                            NSLog(@"%@", fsTitle.text);
                                            flightStatusObj.flightID = fsTitle.text;
                                            flightID = fsTitle.text;
                                        }
                                        
                                    }
                                }
                                
                            }
                            if(tableCounter ==1)
                            {
                                NSLog(@"FLIGHT DETAILS HTML = %@", flightData.html);
                                //NSLog(@"FLIGHT DETAILS TEXT = %@", flightData.text);
                                
                                NSArray *flightStatusRow = [flightData elementsWithTag:GUMBO_TAG_TD];
                                
                                int dataCounter =0;
                                
                                for (OGElement * flightStatusRowData in flightStatusRow)
                                {
                                    
                                    NSLog(@"%D : %@",  dataCounter,flightStatusRowData.text);
                                    
                                    if (!([flightStatusRowData.text rangeOfString:@"On-time"].location == NSNotFound)
                                        || !([flightStatusRowData.text rangeOfString:@"Delayed"].location == NSNotFound)
                                        || !([flightStatusRowData.text rangeOfString:@"Departed"].location == NSNotFound)
                                        || !([flightStatusRowData.text rangeOfString:@"Canceled"].location == NSNotFound)
                                        ) {
                                        flightStatusObj = [[FlightStatus alloc] init];
                                        //NSLog(@"On-time");
                                        OGElement *fsStatus = (OGElement*) flightStatusRow[dataCounter];
                                        flightStatusObj.status = fsStatus.text;
                                        
                                        OGElement *fsStatusMsg = (OGElement*) flightStatusRow[dataCounter + 1];
                                        flightStatusObj.statusMessage = fsStatusMsg.text;
                                        
                                        OGElement *fsDepartureOrArrival = (OGElement*) flightStatusRow[dataCounter + 2];
                                        
                                    }
                                    
                                    
                                    
                                    OGElement *fsDepartureOrArrival = (OGElement*) flightStatusRow[dataCounter];
                                    if (!([fsDepartureOrArrival.text rangeOfString:@"Departure"].location == NSNotFound)) {
                                        if(dataCounter <= flightStatusRow.count)
                                        {
                                            flightStatusObj.departureAirportCodeText = fsDepartureOrArrival.text;
                                            OGElement *fsDepartureOrArrivalCode = (OGElement*) flightStatusRow[dataCounter + 1];
                                            flightStatusObj.departureAirportCode = fsDepartureOrArrivalCode.text;
                                            OGElement *fsDepartureDateTime = (OGElement*) flightStatusRow[dataCounter + 2];
                                            flightStatusObj.departureDateTime = fsDepartureDateTime.text;
                                            OGElement *fsDepartureScheduledDateTime = (OGElement*) flightStatusRow[dataCounter + 3];
                                            if (!([fsDepartureScheduledDateTime.text rangeOfString:@"(scheduled"].location == NSNotFound)) {
                                                flightStatusObj.departureScheduledTimeText = fsDepartureScheduledDateTime.text;
                                            }
                                            
                                            OGElement *fsTerminal = (OGElement*) flightStatusRow[dataCounter +4];
                                            if (!([fsTerminal.text rangeOfString:@"Terminal"].location == NSNotFound)) {
                                                flightStatusObj.departureTerminal = fsTerminal.text;
                                            }
                                            
                                            
                                            OGElement *fsCity = (OGElement*) flightStatusRow[dataCounter + 6];
                                            flightStatusObj.departureCity = fsCity.text;
                                            
                                            OGElement *fsDateText = (OGElement*) flightStatusRow[dataCounter + 7];
                                            flightStatusObj.departureDate = fsDateText.text;
                                            if(dataCounter + 9 < flightStatusRow.count)
                                            {
                                                OGElement *fsGate = (OGElement*) flightStatusRow[dataCounter + 9];
                                                if (!([fsGate.text rangeOfString:@"Gate"].location == NSNotFound)) {
                                                    flightStatusObj.departureGate = fsGate.text;
                                                }
                                            }
                                            
                                        }
                                        
                                        
                                    }
                                    
                                    if (!([flightStatusRowData.text rangeOfString:@"Arrival"].location == NSNotFound)) {
                                        if(dataCounter <= flightStatusRow.count)
                                        {
                                            flightStatusObj.arrivalAirportText = fsDepartureOrArrival.text;
                                            OGElement *fsDepartureOrArrivalCode = (OGElement*) flightStatusRow[dataCounter + 1];
                                            flightStatusObj.arrivalAirportCode = fsDepartureOrArrivalCode.text;
                                            OGElement *fsArrivalDateTime = (OGElement*) flightStatusRow[dataCounter + 2];
                                            flightStatusObj.arrivalDateTime = fsArrivalDateTime.text;
                                            OGElement *fsArrivalScheduledDateTime = (OGElement*) flightStatusRow[dataCounter + 3];
                                            
                                            if (!([fsArrivalScheduledDateTime.text rangeOfString:@"(scheduled"].location == NSNotFound)) {
                                                flightStatusObj.arrivalScheduledTimeText = fsArrivalScheduledDateTime.text;
                                            }
                                            
                                            OGElement *fsTerminal = (OGElement*) flightStatusRow[dataCounter + 4];
                                            if (!([fsTerminal.text rangeOfString:@"Terminal"].location == NSNotFound)) {
                                                flightStatusObj.departureTerminal = fsTerminal.text;
                                            }
                                            
                                            OGElement *fsCity = (OGElement*) flightStatusRow[dataCounter + 6];
                                            flightStatusObj.arrivalCity = fsCity.text;
                                            
                                            OGElement *fsDateText = (OGElement*) flightStatusRow[dataCounter + 7];
                                            flightStatusObj.arrivalDate = fsDateText.text;
                                            if(dataCounter + 9 < flightStatusRow.count)
                                            {
                                                OGElement *fsGate = (OGElement*) flightStatusRow[dataCounter + 9];
                                                if (!([fsGate.text rangeOfString:@"Gate"].location == NSNotFound)) {
                                                    flightStatusObj.arrivalGate = fsGate.text;
                                                }
                                            }
                                            
                                            
                                        }
                                    }
                                    flightStatusObj.flightID = flightID;
                                    if(flightStatusObj.arrivalAirportCode != nil)
                                    {
                                        [flightStatusARR addObject:flightStatusObj];
                                        flightStatusObj = nil;
                                    }
                                    
                                    dataCounter++;
                                    
                                }
                                
                            }
                            
                            tableCounter++;
                        }
                        
                        
                    }
                }
                counter++;
            }
            
        }
        
        NSLog(@"Count of Flight status %lu", (unsigned long)flightStatusARR.count);
    }
    return flightStatusARR;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    
    NSString *fullURL = @"https://www.google.com/search?site=&source=hp&ei=y79NU7-pOsqusATS8YGQBw&q=ua302&oq=ua302&gs_l=mobile-gws-hp.3..0l2j0i30l2j0i5i30.8707.11504.0.12633.6.6.0.2.2.0.315.1095.1j2j2j1.6.0....0...1c.1.41.mobile-gws-hp..1.5.654.0.akbJl-seCfs";
    
    NSArray *flightStatusArray;
    flightStatusArray = [self parseFlightStatus:fullURL];
    
  
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
