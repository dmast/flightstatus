//
//  Utility.h
//  FlightStatus
//
//  Created by David M Mast on 5/21/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utility : NSObject


+ (NSString*)getDepartureText;
+ (void)setDepartureText:(NSString*)text;

+ (NSString*)getCarrierText;
+ (void)setCarrierText:(NSString*)text;

+ (NSString*)getArrivalText;
+ (void)setArrivalText:(NSString*)text;

+ (BOOL)isArrivalSet;
+ (void)setArrival:(BOOL)value;

+ (BOOL)isDepartureSet;
+ (void)setDeparture:(BOOL)value;

+ (BOOL)isCarrierSet;
+ (void)setCarrier:(BOOL)value;

/**
 BOOL updateArrivalText;
 BOOL updateDepartureText;
 BOOL updateCarrierText;
**/
@end
