//
//  ViewController.m
//  EmbeddedSwapping
//
//  Created by David Mast on 5/23/2014..
//
//

#import "MainViewController.h"
#import "ContainerViewController.h"
#import "MPAdView.h"
#import "MopubManager.h"

@interface MainViewController () <MPAdViewDelegate>
@property (nonatomic, retain) MPAdView *adView;
@property (nonatomic, weak) ContainerViewController *containerViewController;
- (IBAction)swapButtonPressed:(id)sender;
@end

@implementation MainViewController

- (UIViewController *)viewControllerForPresentingModalView {
    return self;
}

- (void)viewDidLoad
{
    
    self.adView = MopubManager.SingletonMopubPhoneBannerAdview;
    self.adView.delegate = self;
    self.adView.frame = CGRectMake(0, (self.view.frame.size.height - MOPUB_BANNER_SIZE.height),MOPUB_BANNER_SIZE.width, MOPUB_BANNER_SIZE.height);
    [self.view addSubview:self.adView];
    [self.adView loadAd];
    [super viewDidLoad];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);

    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSLog(@"%s", __PRETTY_FUNCTION__);

    if ([segue.identifier isEqualToString:@"embedContainer"]) {
        self.containerViewController = segue.destinationViewController;
    }
}

- (IBAction)swapButtonPressed:(id)sender
{
    [self.containerViewController swapViewControllers];
}

@end
