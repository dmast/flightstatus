//
//  FlightStatus.h
//  FlightStatusApp
//
//  Created by David Mast on 3/12/2014.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FlightStatus : NSObject

@property (nonatomic, strong) NSString *flightID; // name of FlightStatus
@property (nonatomic, strong) NSString *flightIDCode;
@property (nonatomic, strong) NSString *prepTime; // preparation time
@property (nonatomic, strong) NSString *image; // image filename of FlightStatus
@property (nonatomic, strong) NSArray *ingredients; // ingredients of FlightStatus

@property (nonatomic, retain) NSString *carrier;
@property (nonatomic, retain) NSString *status;
@property (nonatomic, retain) NSString *statusMessage;
@property (nonatomic, retain) NSString *arrivalAirportText;
@property (nonatomic, retain) NSString *arrivalAirportCode;

@property (nonatomic, retain) NSString *departureAirportCodeText;
@property (nonatomic, retain) NSString *departureAirportCode;
@property (nonatomic, retain) NSString *arrivalCity;
@property (nonatomic, retain) NSString *departureCity;
@property (nonatomic, retain) NSString *arrivalDateTime;
@property (nonatomic, retain) NSString *arrivalDate;
@property (nonatomic, retain) NSString *departureDateTime;
@property (nonatomic, retain) NSString *departureDate;
@property (nonatomic, retain) NSString *arrivalScheduledTimeText;
@property (nonatomic, retain) NSString *departureScheduledTimeText;

@property (nonatomic, retain) NSString *arrivalTerminal;
@property (nonatomic, retain) NSString *departureTerminal;
@property (nonatomic, retain) NSString *arrivalGate;
@property (nonatomic, retain) NSString *departureGate;

@end
