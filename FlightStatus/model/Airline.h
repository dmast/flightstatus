//
//  Airline.h
//  FlightStatus
//
//  Created by David M Mast on 5/19/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Airline : NSManagedObject

@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *code;

@end
