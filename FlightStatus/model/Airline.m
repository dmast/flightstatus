//
//  Airline.m
//  FlightStatus
//
//  Created by David M Mast on 5/19/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "Airline.h"


@implementation Airline

@dynamic name;
@dynamic code;

@end
