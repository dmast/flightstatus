//
//  Airline+Manage.m
//  FlightStatus
//
//  Created by David M Mast on 5/19/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "Airline+Manage.h"
#import "Airline.h"

@implementation Airline (Manage)

+ (Airline *)addAirlineWithData:(NSString *)code name:(NSString *)name context:(NSManagedObjectContext *)context {
    Airline *airline = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Airline"];
    request.predicate = [NSPredicate predicateWithFormat:@"code = %@", code];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"code" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    NSError *error = nil;
    NSArray *airlines = [context executeFetchRequest:request error:&error];
    
    if (!airlines || ([airlines count] > 1)) {
        // handle error
    } else if (![airlines count]) {
        airline = [NSEntityDescription insertNewObjectForEntityForName:@"Airline"
                                                inManagedObjectContext:context];
        airline.code = code;
        airline.name = name;
        
        
    } else {
        airline = [airlines lastObject];
    }
    
    return airline;
    
}
@end
