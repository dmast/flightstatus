//
//  Airport.h
//  FlightStatus
//
//  Created by David M Mast on 5/19/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Airport : NSManagedObject

@property (nonatomic, retain) NSString * citystate;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * code;

@end
