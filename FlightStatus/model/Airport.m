//
//  Airport.m
//  FlightStatus
//
//  Created by David M Mast on 5/19/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "Airport.h"


@implementation Airport

@dynamic citystate;
@dynamic country;
@dynamic code;

@end
