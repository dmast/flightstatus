//
//  Airline+Manage.h
//  FlightStatus
//
//  Created by David M Mast on 5/19/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "Airline.h"

@interface Airline (Manage)
+ (Airline *)addAirlineWithData:(NSString *)code name:(NSString *)name context:(NSManagedObjectContext *)context;

@end
