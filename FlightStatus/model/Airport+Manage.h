//
//  Airport+Manage.h
//  FlightStatus
//
//  Created by David M Mast on 5/19/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "Airport.h"

@interface Airport (Manage)

+ (Airport *)addAirportWithData:(NSString *)code country:(NSString *)country citystate:(NSString *)citystate context:(NSManagedObjectContext *)context;

@end
