//
//  Airport+Manage.m
//  FlightStatus
//
//  Created by David M Mast on 5/19/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "Airport+Manage.h"
#import "Airport.h"

@implementation Airport (Manage)

+ (Airport *)addAirportWithData:(NSString *)code country:(NSString *)country citystate:(NSString *)citystate context:(NSManagedObjectContext *)context
{
    Airport *airport = nil;
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Airport"];
    request.predicate = [NSPredicate predicateWithFormat:@"code = %@", code];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"code" ascending:YES];
    request.sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    
    NSError *error = nil;
    NSArray *airports = [context executeFetchRequest:request error:&error];
    
    if (!airports || ([airports count] > 1)) {
        // handle error
    } else if (![airports count]) {
        airport = [NSEntityDescription insertNewObjectForEntityForName:@"Airport"
                                                   inManagedObjectContext:context];
        airport.code = code;
        airport.citystate = citystate;
        airport.country = country;
        
    } else {
        airport = [airports lastObject];
    }
    
    return airport;
}


@end
