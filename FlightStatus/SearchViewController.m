//
//  SearchViewController.m
//  FlightStatus
//
//  Created by David M Mast on 5/21/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "SearchViewController.h"
#import "FlightStatusTableViewController.h"
#import "Utility.h";

@interface SearchViewController ()<UITableViewDataSource,UITableViewDelegate, UISearchBarDelegate>    {
    NSArray *airports;
    NSArray *airlines;
    NSArray *tableData;
    NSString *selectedText;
    
        
}

@end
@implementation SearchViewController
@synthesize isSettingArrival;
@synthesize isSettingCarrier;
@synthesize isSettingDeparture;
@synthesize isSearchForAirlines;
@synthesize delegate;
@synthesize FlightStatusArray;

- (void) performFetchForAirports
{
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"FlightStatus-Info" ofType:@"plist"]];
    NSLog(@"dictionary = %@", dictionary);
    NSArray *array = [dictionary objectForKey:@"AirportsData"];
    NSLog(@"array = %@", array);
    
    // Build the array from the plist
    airports = array;
    tableData = array;
}
- (void) performFetchForAirlines
{
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"FlightStatus-Info" ofType:@"plist"]];
    NSLog(@"dictionary = %@", dictionary);
    NSArray *array = [dictionary objectForKey:@"AirlinesData"];
    NSLog(@"array = %@", array);
    
    // Build the array from the plist
    airlines = array;
    tableData = array;
}
- (void) filteredContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSLog(@"User started searching");
    
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBarInstance
{
    NSLog(@"User searched for %@", searchBarInstance.text);
    
}
-(void)searchBar:(UISearchBar*)searchBarInstance textDidChange:(NSString*)text
{
    NSLog(@"performing query on search");
    
    NSLog(@"Text Changed %@", searchBarInstance.text);
    //NSLog(@"Number of Fetched results %d", _fetchedResultsController.fetchedObjects.count);
    //_channelData = [[NSMutableArray alloc] init];
    //recover query
    NSString *query = searchBar.text;
    if(query && query.length)
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", query];
        if(self.isSearchForAirlines == 1)
        {
            tableData = [airlines filteredArrayUsingPredicate:predicate];
        }
        else
        {
            tableData = [airports filteredArrayUsingPredicate:predicate];
        }
        
    }
    
    [tblSearch reloadData];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBarInstance {
    [self handleSearch:searchBarInstance];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBarInstance {
    [self handleSearch:searchBarInstance];
}

- (void)handleSearch:(UISearchBar *)searchBarInstance {
    NSLog(@"User searched for %@", searchBarInstance.text);
    [searchBarInstance resignFirstResponder]; // if you want the keyboard to go away
}

- (void)searchBarCancelButtonClicked:(UISearchBar *) searchBarInstance {
    NSLog(@"User canceled search");
    [searchBarInstance resignFirstResponder]; // if you want the keyboard to go away
}
-(void)viewWillLayoutSubviews
{
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    tblSearch.delegate = self;
    tblSearch.dataSource = self;
    searchBar.delegate = self;
    //self.isSearchForAirlines = false;
    if(self.isSearchForAirlines)
    {
        [self performFetchForAirlines];
        tableData = airlines;
    }
    else
    {
        [self performFetchForAirports];
        tableData = airports;
    }
    selectedText = @"";
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.navigationController.navigationBar.barTintColor = [[UIColor alloc] initWithRed:0.08235294117647059 green:0.5803921568627451 blue:0.16862745098039217 alpha:.7];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    //self.navigationController.navigationBar.translucent = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *selectedCell=[tableView cellForRowAtIndexPath:indexPath];
    NSString * selectedData = selectedCell.textLabel.text;
    NSLog(@"%@", selectedData);
    selectedText = selectedData;
    
    
    [self performSegueWithIdentifier:@"segueSearchToRoot" sender:self];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"CustomCell";
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc]
                initWithStyle:UITableViewCellStyleDefault
                reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell.
    cell.textLabel.text = [tableData objectAtIndex: [indexPath row]];
    return cell;

}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
**/
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"segueSearchToRoot"]) {
        UINavigationController *navigationController = (UINavigationController *)segue.destinationViewController;
        FlightStatusTableViewController *vc = [[navigationController viewControllers] lastObject];
        if(self.isSettingArrival)
        {
            vc.isSettingArrival = YES;
            vc.selectedDataFromSearch = selectedText;
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DoUpdateArrival" object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:selectedText, @"String", nil]];
            [Utility setArrivalText:selectedText];
            [Utility setArrival:YES];
        }
        else if(self.isSettingDeparture)
        {
            vc.isSettingDeparture = YES;
            vc.selectedDataFromSearch = selectedText;
           
            [[NSNotificationCenter defaultCenter] postNotificationName:@"DoUpdateDeparture" object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:selectedText, @"String", nil]];
            [Utility setDepartureText:selectedText];
            [Utility setDeparture:YES];
        }
        else if(isSettingCarrier)
        {
            vc.isSettingCarrier = true;
            vc.selectedDataFromSearch = selectedText;
             [[NSNotificationCenter defaultCenter] postNotificationName:@"DoUpdateCarrier" object:self userInfo:[NSDictionary dictionaryWithObjectsAndKeys:selectedText, @"String", nil]];
            [Utility setCarrierText:selectedText];
            [Utility setCarrier:YES];
            
        }
        vc.FlightStatusArray = self.FlightStatusArray;
        NSString *itemToPassBack = selectedText;
        NSLog(@"%@",selectedText);
        [self.delegate selectedFlightData:self didFinishSelectingData:selectedText isSettingArrival:(self.isSettingArrival) isSettingDeparture:self.isSettingDeparture isSettingCarrier:self.isSettingCarrier];
        
       
        //vc.FlightStatusArray = self.searchFlightStatusArray;
        
        
        
    }
    
}


- (IBAction)btnBackClicked:(id)sender {
    NSLog(@"Back Button Clicked");
    
    [self performSegueWithIdentifier:@"segueSearchToRoot" sender:self];
}
@end
