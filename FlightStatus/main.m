//
//  main.m
//  CustomTable
//
//  Created by Simon on 7/12/13.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "FlightStatusAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FlightStatusAppDelegate class]));
    }
}
