//
//  CustomTableCell.m
//  CustomTable
//
//  Created by David Mast on 3/12/2014.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "FlightStatusTableCell.h"

@implementation FlightStatusTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
