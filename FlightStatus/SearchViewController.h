//
//  SearchViewController.h
//  FlightStatus
//
//  Created by David M Mast on 5/21/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SearchViewController;
@protocol SearchViewControllerDelegate <NSObject>

-(void)selectedFlightData:(SearchViewController*)controller didFinishSelectingData:(NSString*)data isSettingArrival:(BOOL)isArrival isSettingDeparture:(BOOL)isDeparture isSettingCarrier:(BOOL)isCarrier;

@end
@interface SearchViewController : UIViewController
{
    IBOutlet UISearchBar *searchBar;
    IBOutlet UITableView *tblSearch;
    IBOutlet UIBarButtonItem *btnBack;
    
}
- (IBAction)btnBackClicked:(id)sender;
@property (nonatomic) BOOL isSearchForAirlines;

@property (nonatomic) BOOL isSettingDeparture;
@property (nonatomic) BOOL isSettingArrival;
@property (nonatomic) BOOL isSettingCarrier;
@property (strong, nonatomic) NSArray *FlightStatusArray;

@property (nonatomic, weak) id <SearchViewControllerDelegate> delegate;

@end
