//
//  EmptySegue.m
//  EmbeddedSwapping
//
//  Created by David Mast on 5/23/2014..
// JMT Apps All rights reserved
//

#import "EmptySegue.h"

@implementation EmptySegue

- (void)perform
{
    // Nothing. The ContainerViewController class handles all of the view
    // controller action.
}

@end
