//
//  CustomTableViewController.h
//  CustomTable
//
//  Created by David Mast on 3/12/2014.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SSSearchBar.h"
#import "Airline.h"
#import "Airport.h"
#import <CoreData/CoreData.h>
#import <QuartzCore/QuartzCore.h>

@interface FlightStatusTableViewController : UITableViewController <UISearchBarDelegate,NSFetchedResultsControllerDelegate> {
    //NSFetchedResultsController *fetchedResultsController;
    
}
@property (strong, nonatomic) IBOutlet UIImageView *btnDeparture;
@property (strong, nonatomic) IBOutlet UIImageView *btnArrival;

@property (strong, nonatomic) IBOutlet UIImageView *btnCarrier;

- (IBAction)btnDepartureTouchDown:(id)sender;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *FlightStatusArray;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (strong, nonatomic) IBOutlet UIButton *btnSearch;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *departureTap;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *arrivalTap;
- (IBAction)destinationTapped:(id)sender;
- (IBAction)arrivalTapped:(id)sender;
@property (strong, nonatomic) IBOutlet UITapGestureRecognizer *carrierTap;

- (IBAction)carrierDidTap:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *lblCarrier;
@property (strong, nonatomic) IBOutlet UILabel *lblDeparture;
@property (strong, nonatomic) IBOutlet UILabel *lblArrival;

@property (strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
- (IBAction)departureTouchUpInside:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *departureTransparentView;
- (IBAction)searchClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIView *arrivalTransparentView;
@property (strong, nonatomic) IBOutlet UIView *carrierTransparentView;
@property (strong, nonatomic) NSString *selectedDataFromSearch;

@property (nonatomic) BOOL isSettingDeparture;
@property (nonatomic) BOOL isSettingArrival;
@property (nonatomic) BOOL isSettingCarrier;


@end
