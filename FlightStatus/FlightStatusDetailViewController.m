//
//  FlightStatusDetailViewController.m
//  FlightStatusApp
//
//  Created by David Mast on 3/12/2014.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "FlightStatusDetailViewController.h"
#import "AboutViewController.h"
#import "ObjectiveGumbo.h"
#import "FlightStatus.h"
#import "FlightStatusDetailViewCell.h"
#import "FlightStatusTableViewController.h"

@interface FlightStatusDetailViewController ()

@end

@implementation FlightStatusDetailViewController
@synthesize searchFlightStatusArray;
@synthesize tableView;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (IBAction)backButtonClicked:(id)sender
{
    NSLog(@"Back Button Clicked");

    [self performSegueWithIdentifier:@"segueToRoot" sender:self];

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"segueToRoot"]) {
        UINavigationController *navigationController = (UINavigationController *)segue.destinationViewController;
        FlightStatusTableViewController *vc = [[navigationController viewControllers] lastObject];
        vc.FlightStatusArray = self.searchFlightStatusArray;
        
        
    }
   
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	//self.title = self.FlightStatus.flightID;
    //self.prepTimeLabel.text = self.FlightStatus.prepTime;
    //self.FlightStatusImageView.image = [UIImage imageNamed:self.FlightStatus.image];
    /**
    NSMutableString *ingredientsText = [NSMutableString string];
    for (NSString* ingredient in self.FlightStatus.ingredients) {
        [ingredientsText appendFormat:@"%@\n", ingredient];
    }
    self.ingredientsTextView.text = ingredientsText;
     **/
    //[self parseFlightStatus(flightURL) ];
    //tableView.delegate = self;
    //[tableView reloadData];
    NSLog(@"%lu", (unsigned long)[searchFlightStatusArray count]);
    self.tableView.delegate = self;
    self.navigationController.navigationBar.barTintColor = [[UIColor alloc] initWithRed:0.08235294117647059 green:0.5803921568627451 blue:0.16862745098039217 alpha:.7];
    [self.navigationController.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    //self.navigationController.navigationBar.translucent = NO;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return [searchFlightStatusArray count];
    
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"flightStatusCell";
    FlightStatusDetailViewCell *cell = (FlightStatusDetailViewCell *)[self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    // Configure the cell...
    if (cell == nil) {
        cell = [[FlightStatusDetailViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Display FlightStatus in the table cell
    FlightStatus *flightStatus = nil;
    
    flightStatus = [searchFlightStatusArray objectAtIndex:indexPath.row];
    
    cell.flightID.text = flightStatus.flightID;
    cell.flightStatus.text = flightStatus.status;
    
    //NSLog(@"%@", cell.flightID.text);
    cell.departsInText.text = [NSString stringWithFormat:@"Departs at %@ %@",flightStatus.departureDate,flightStatus.departureDateTime ];
    cell.departureAirportCode.text = flightStatus.departureAirportCode;
    cell.arrivalAirportCode.text = flightStatus.arrivalAirportCode;
    cell.departureCity.text = [NSString stringWithFormat:@"Departs %@", flightStatus.departureCity];
    cell.departureDate.text = flightStatus.departureDate;
    cell.departureTime.text = flightStatus.departureDateTime;
    NSString *strDepartureTerminal = flightStatus.departureTerminal;
    
    strDepartureTerminal = [strDepartureTerminal stringByReplacingOccurrencesOfString:@"T-"
                                         withString:@""];
    cell.departureTerminal.text = strDepartureTerminal;
    cell.departureGate.text = flightStatus.departureGate;
    cell.arrivalCity.text = [NSString stringWithFormat:@"Arrives %@", flightStatus.arrivalCity];
    cell.arrivalDate.text = flightStatus.arrivalDate;
    cell.arrivalTime.text = flightStatus.arrivalDateTime;
    NSString *strArrivalTerminal = flightStatus.arrivalTerminal;
    
    strArrivalTerminal = [strArrivalTerminal stringByReplacingOccurrencesOfString:@"T-" withString:@""];
    cell.arrivalTerminal.text = strArrivalTerminal;
    
    cell.arrivalGate.text = flightStatus.arrivalGate;
    
    if (!([flightStatus.status rangeOfString:@"On-time"].location == NSNotFound)
        || !([flightStatus.status rangeOfString:@"Departed"].location == NSNotFound)
        || !([flightStatus.status rangeOfString:@"Landed"].location == NSNotFound)
        ) {
        //[cell.flightImage setImage:[UIImage imageNamed: @"onTimeFlightImage.png"]];
        cell.flightStatus.textColor = [UIColor greenColor];
    }
    else if([flightStatus.status isEqualToString:@"Scheduled"])
    {
        cell.flightStatus.textColor = [UIColor greenColor];
    }
    else
    {
        //[cell.flightImage setImage:[UIImage imageNamed: @"delayedFlightImage.png"]];
        cell.flightStatus.textColor = [UIColor redColor];
        //NSString *newStatus = [[NSString alloc] init];
        
        
        //cell.flightStatus.text = [cell.flightStatus.text stringByAppendingString:[@" %@", flightStatus.status];
    }
    if(!([flightStatus.status rangeOfString:@" min"].location == NSNotFound))
    {
        cell.flightStatus.textColor = [UIColor redColor];
        cell.flightStatus.text = [cell.flightStatus.text stringByAppendingString:@" - Delayed"];
    }
    
    
    
    
    
    return cell;
}


@end
