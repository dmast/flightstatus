//
//  MopubManager.m
//  FlightStatus
//
//  Created by David M Mast on 5/23/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "MopubManager.h"

@implementation MopubManager

#pragma mark Singleton Methods

+ (id)sharedMopubManager {
    static MopubManager *sharedMopubManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMopubManager = [[self alloc] init];
        
    });
    return sharedMopubManager;
}

+ (id)SingletonMopubPhoneBannerAdview {
    static MPAdView *mpAdview = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        mpAdview = [[MPAdView alloc] initWithAdUnitId:@"bb0c6736fb264dfd8d615f7cb6ecacef"
                                             size:MOPUB_BANNER_SIZE];
        
        
    });
    return mpAdview;
}

+ (id)SingletonMopubTabletBannerAdview {
    static MPAdView *mpAdview = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        mpAdview = [[MPAdView alloc] initWithAdUnitId:@"5707b802f9e240e7a8586c3e6c3e9d47"
                                             size:MOPUB_BANNER_SIZE];
    });
    return mpAdview;
}


- (id)init {
    if (self = [super init]) {
        
    }
    return self;
}


@end
