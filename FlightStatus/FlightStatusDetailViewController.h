//
//  FlightStatusDetailViewController.h
//  FlightStatusApp
//
//  David Mast on 3/12/2014.
//  Copyright (c) 2013 JMTApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FlightStatus.h"
#import "FlightStatusDetailViewCell.h"

@interface FlightStatusDetailViewController : UITableViewController
@property (strong, nonatomic) NSArray *searchFlightStatusArray;


@property (strong, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) IBOutlet UIButton *backButton;

- (IBAction)backButtonClicked:(id)sender;

@end
