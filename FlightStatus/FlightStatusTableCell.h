//
//  FlightStatusTableCell.h
//  FlightStatusApp
//
//  Created by David Mast on 3/12/2014.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlightStatusTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *flightID;
@property (weak, nonatomic) IBOutlet UILabel *flightStatus;

@property (strong, nonatomic) IBOutlet UILabel *flightStatusDate;


@end
