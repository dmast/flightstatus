# README #



### What is this repository for? ###

* Simple Container View implementation so that banner ads from mopub are not reloaded on every View controller viewDidLoad() call. Essentially the container swaps view controllers, so new banner ads are only shown every 60 seconds.
