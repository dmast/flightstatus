//
//  FlightStatusTableViewCell.h
//  FlightStatus
//
//  Created by David M Mast on 4/24/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FlightStatusDetailViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *flightID;
@property (weak, nonatomic) IBOutlet UILabel *flightStatus;
@property (weak, nonatomic) IBOutlet UILabel *departsInText;
@property (weak, nonatomic) IBOutlet UILabel *departureAirportCode;
@property (weak, nonatomic) IBOutlet UILabel *arrivalAirportCode;
@property (weak, nonatomic) IBOutlet UILabel *departureCity;
@property (weak, nonatomic) IBOutlet UILabel *departureDate;
@property (weak, nonatomic) IBOutlet UILabel *departureTime;
@property (weak, nonatomic) IBOutlet UILabel *departureTerminal;
@property (weak, nonatomic) IBOutlet UILabel *departureGate;
@property (weak, nonatomic) IBOutlet UILabel *arrivalCity;
@property (weak, nonatomic) IBOutlet UILabel *arrivalDate;
@property (weak, nonatomic) IBOutlet UILabel *arrivalTime;
@property (weak, nonatomic) IBOutlet UILabel *arrivalTerminal;

@property (weak, nonatomic) IBOutlet UILabel *arrivalGate;
@property (strong, nonatomic) IBOutlet UIImageView *flightImage;


@end


