//
//  AirportSearchVC.m
//  FlightStatus
//
//  Created by David M Mast on 5/20/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import "AirportSearchVC.h"

@interface AirportSearchVC ()
@property (strong, nonatomic) Airline *airline;
@property (strong, nonatomic) Airport *airport;
@end
@implementation AirportSearchVC {
    NSMutableArray *airlines;
    NSMutableArray *airports;
    NSMutableArray *filteredAirports;
}

@synthesize autocompleteTableView;
@synthesize queryType;

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    
    // Put anything that starts with this substring into the autocompleteUrls array
    // The items in this array is what will show up in the table view
    [filteredAirports removeAllObjects];
    for(NSString *curString in airlines) {
        NSRange substringRange = [curString rangeOfString:substring];
        if (substringRange.location == 0) {
            [filteredAirports addObject:curString];
        }
    }
    autocompleteTableView.dataSource = [[NSArray alloc] initWithArray:filteredAirports];
    [autocompleteTableView reloadData];
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if(textField.tag <= 2)
    {
            autocompleteTableView.hidden = NO;
            
            NSString *substring = [NSString stringWithString:textField.text];
            substring = [substring stringByReplacingCharactersInRange:range withString:string];
            [self searchAutocompleteEntriesWithSubstring:substring];
            return YES;
    }
    return YES;
}

- (void) performFetchForAirports
{
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"FlightStatus-Info" ofType:@"plist"]];
    NSLog(@"dictionary = %@", dictionary);
    NSMutableArray *array = [dictionary objectForKey:@"AirportsData"];
    NSLog(@"array = %@", array);
    
    // Build the array from the plist
    airports = array;
}
- (void) performFetchForAirlines
{
    NSDictionary *dictionary = [NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"FlightStatus-Info" ofType:@"plist"]];
    NSLog(@"dictionary = %@", dictionary);
    NSMutableArray *array = [dictionary objectForKey:@"AirlinesData"];
    NSLog(@"array = %@", array);
    
    // Build the array from the plist
    airlines = array;


}

- (void)viewDidLoad
{
    [super viewDidLoad];
   
    if([self.queryType isEqualToString:@"Airport"])
    {
         [self performFetchForAirports];
    }
    else
    {
        [self performFetchForAirlines];
    }

    autocompleteTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 80, 320, 120) style:UITableViewStylePlain];
    autocompleteTableView.delegate = self;
    autocompleteTableView.dataSource = self;
    autocompleteTableView.scrollEnabled = YES;
    autocompleteTableView.hidden = YES;
    [self.view addSubview:autocompleteTableView];
    
    //FlightStatusAppDelegate *appDelegate = (FlightStatusAppDelegate *)[[UIApplication sharedApplication] delegate];
    //NSManagedObjectContext *context = appDelegate.managedObjectContext;
    //self.managedObjectContext = context;
    //[self performFetchForAirlines];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField {
    textField.placeholder = textField.text;
    textField.text = @"";
    if(textField.tag == 1)
    {
    }
    if(textField.tag == 2)
    {
        
    }
    if(textField.tag == 3)
    {
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark UITextFieldDelegate methods



#pragma mark UITableViewDataSource methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger) section {
    return airlines.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = nil;
    static NSString *AutoCompleteRowIdentifier = @"AutoCompleteRowIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
    
    cell.textLabel.text = [airlines objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *selectedCell = [tableView cellForRowAtIndexPath:indexPath];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
