//
//  AirportSearchVC.h
//  FlightStatus
//
//  Created by David M Mast on 5/20/14.
//  Copyright (c) 2014 JMT Apps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Airline+Manage.h"
#import "Airport+Manage.h"
#import <CoreData/CoreData.h>
#import "FlightStatusAppDelegate.h"

@interface AirportSearchVC : UIViewController<UITextFieldDelegate,UITableViewDataSource, UITableViewDelegate> {
    
    
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring;
@property (strong, nonatomic) IBOutlet UITableView *autocompleteTableView;
@property (nonatomic, strong) NSString *queryType;

@end
